import React, { useState } from "react";
// import "./homePage.css";
import "bootstrap/dist/css/bootstrap.css";

import "../App.css";

function GameHistory() {
  const accessToken = localStorage.getItem("accessToken");
  const [histories, setHistories] = useState([
    {
      status: "win",
      roomName: "Player Vs Computer",
      date: "11-6-2023",
      time: "06:00:01",
    },
    {
      status: "draw",
      roomName: "Player Vs Computer",
      date: "11-6-2023",
      time: "06:11:21",
    },
    {
      status: "lose",
      roomName: "Player Vs Computer",
      date: "11-6-2023",
      time: "06:11:25",
    },
    {
      status: "draw",
      roomName: "Player Vs Computer",
      date: "11-6-2023",
      time: "06:11:34",
    },
    {
      status: "draw",
      roomName: "Player Vs Computer",
      date: "11-6-2023",
      time: "06:11:37",
    },
    {
      status: "draw",
      roomName: "Player Vs Computer",
      date: "11-6-2023",
      time: "06:11:45",
    },
    {
      status: "win",
      roomName: "Player Vs Computer",
      date: "11-6-2023",
      time: "06:11:52",
    },
    {
      status: "lose",
      roomName: "Player Vs Computer",
      date: "11-6-2023",
      time: "06:12:02",
    },
    {
      status: "draw",
      roomName: "Player Vs Computer",
      date: "11-6-2023",
      time: "06:12:08",
    },
    {
      status: "win",
      roomName: "Player Vs Computer",
      date: "11-6-2023",
      time: "06:12:19",
    },
  ]);
  return (
    <div className="tableContainer container ">
      <h3 className="title fs-3 text-start ms-2 text-center">Game History</h3>
      <table className="table table-fixed">
        <thead>
          <tr>
            <th className="col-xl-3 col-xs-3 text-center">Username</th>
            <th className="col-xl-2 col-xs-3 text-center">Status</th>
            <th className="col-xl-3 col-xs-3 text-center">Room Name</th>
            <th className="col-xl-2 col-xs-3 text-center">Date</th>
            <th className="col-xl-2 col-xs-3 text-center">Time</th>
          </tr>
        </thead>
        <tbody>
          {histories.map((history, i) => {
            return (
              <tr key={i}>
                <td className="col-xl-3 col-xs-3 text-center">{accessToken}</td>
                <td className="col-xl-2 col-xs-3 text-center">
                  {history.status}
                </td>
                <td className="col-xl-3 col-xs-3 text-center">
                  {history.roomName}
                </td>
                <td className="col-xl-2 col-xs-2 text-center">
                  {history.date}
                </td>
                <td className="col-xl-2 col-xs-2 text-center">
                  {history.time}
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default GameHistory;
